const mongoose = require('mongoose');
const shortid = require('shortid');
const time = require('./../libs/timeLib');
const response = require('./../libs/responseLib')
const logger = require('./../libs/loggerLib');
const validateInput = require('../libs/paramsValidationLib')
const check = require('../libs/checkLib')
const {isNullOrUndefined} = require('util');
const tokenLib = require('../libs/tokenLib')

const moment = require('moment')
var cron = require('node-cron');

var nodeMailer = require('nodemailer');


/* Models */
const UserModel = mongoose.model('User');
const Campaign = mongoose.model('Campaign');
const Budget = mongoose.model('Budget');
const userMonthlyBudget = mongoose.model('UserMonthlyBudget');
const CampaignActivity = mongoose.model('CampaignActivity');

// create Campaign function
let createCampaign = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.body.campaignUrl && req.body.description) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "CampaignUrl, Description missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let createCampaigns = () => {
        console.log("createCampaign");
        return new Promise((resolve, reject) => {
            let body = {};
            body['campaignId'] = shortid.generate();
            body['campaignUrl'] = req.body.campaignUrl;
            body['description'] = req.body.description;
            body['createdBy'] = req.data._id;
            body['createdOn'] = new Date();
            Campaign.create(body, function (err, response) {
                if (err) {
                    logger.error("Failed to create Campaign", "campaignController => createCampaign()", 5);
                    let apiResponse = response.generate(true, err, 500, null);
                    reject(apiResponse);
                } else {
                    let finalObject = response.toObject();
                    delete finalObject.__v;
                    resolve(finalObject);
                }
            });
        });
    } // end of createCampaigns function

    validatingInputs()
        .then(createCampaigns)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Campaign Created Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of create Campaign function

// get Campaign List function
let getCampaignList = (req, res) => {

    let getSizeLimit = () => {
        console.log("getSizeLimit");
        return new Promise((resolve, reject) => {
            let skip = 0, limit = (req.query.pgSize) ? Number(req.query.pgSize) : 5;
            if (req.query.pg > 0) {
                skip = (limit) * (req.query.pg)
            }
            let data = {}
            data['skip'] = skip;
            data['limit'] = limit;
            resolve(data)
        });
    } // end of getSizeLimit function

    let findCampaign = (data) => {
        console.log("findCampaign");
        return new Promise((resolve, reject) => {
            Campaign.find({}, {}, {
                skip: data.skip,
                limit: data.limit,
                sort: {createdOn: 1}
            })
                .select('-__v -_id')
                .exec((err, campaignDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve campaign", "campaignController => findCampaign()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve campaign", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(campaignDetails)) {
                        logger.error("No campaign found", "campaignController => findCampaign()", 5);
                        let apiResponse = response.generate(true, "No campaign found", 500, null);
                        reject(apiResponse);
                    } else {
                        let final = [];
                        campaignDetails.forEach((item) => {
                            let finalObject = item.toObject();
                            delete finalObject._id;
                            delete finalObject.__v;
                            final.push(finalObject)
                        })
                        final.campaignDetail = final
                        resolve(final);
                    }
                });
        });
    } // end of findCampaign Functions

    let findUser = (data) => {
        console.log("findUser");
        return new Promise((resolve, reject) => {
            let id = [];
            (data.campaignDetail).filter((x) => {
                id.push(x.createdBy)
            })
            UserModel.find({_id: {$in: id}}, {}, {})
                .exec((err, userDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve user", "campaignController => findUser()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve user", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(userDetails)) {
                        logger.error("No user found", "campaignController => findUser()", 5);
                        let apiResponse = response.generate(true, "No user found", 500, null);
                        reject(apiResponse);
                    } else {
                        let final = [];
                        (data.campaignDetail).filter((x) => {
                            userDetails.filter((y) => {
                                if ((x.createdBy).toString() === (y._id).toString()) {
                                    x["username"] = y.name
                                    x["role"] = y.role
                                    final.push(x)
                                }
                            })
                        })
                        resolve(final);
                    }
                });
        });
    } // end of findUser Functions

    let totalCampaign = (final) => {
        console.log("totalCampaign");
        return new Promise((resolve, reject) => {
            Campaign.find({}).count(function (err, cnt) {
                if (err) {
                    logger.error("Failed to retrieve campaign", "campaignController => totalCampaign()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve campaign", 500, null);
                    reject(apiResponse);
                } else {
                    let json;
                    json = {
                        campaign: final,
                        totalRecords: cnt
                    }
                    resolve(json);
                }
            });
        });
    } // end of total Campaign function

    getSizeLimit()
        .then(findCampaign)
        .then(findUser)
        .then(totalCampaign)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Campaign List Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of get Campaign List function

// update campaign function
let updateCampaign = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.body.campaignId) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "campaignId missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let updateCampaigns = () => {
        console.log("updateCampaigns");
        return new Promise((resolve, reject) => {
            let body = {}
            body['campaignUrl'] = req.body.campaignUrl;
            body['description'] = req.body.description;
            Campaign.findOneAndUpdate({campaignId: req.body.campaignId}, body, {new: true})
                .select('-__v -_id')
                .exec((err, campaign) => {
                    if (err) {
                        logger.error("Failed to retrieve campaign data", "cmpaignController => updateCampaigns()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve Campaign", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(campaign)) {
                        logger.error("Failed to update campaign", "campaignController => updateCampaigns()", 5);
                        let apiResponse = response.generate(true, "Failed to update campaign", 500, null);
                        reject(apiResponse);
                    } else {
                        logger.info("Campaign found", "campaignController => updateCampaigns()", 10);
                        resolve(campaign);
                    }
                })
        });
    } // end of updateCampaigns function

    validatingInputs()
        .then(updateCampaigns)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Updated Campaign Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of update Campaign function

// create Campaign Activity function
let createCampaignActivity = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs", req.data);
        return new Promise((resolve, reject) => {
            if (req.body.amount && req.body.description && req.body.campaignId) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "amount, campaignId or description missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let findTeamMember = () => {
        console.log("findUsers");
        return new Promise((resolve, reject) => {
            UserModel.findOne({$and: [{teamMemberId: req.data.teamMemberId}, {role: 'Team Member'}]}, function (err, userDetails) {
                if (err) {
                    logger.error("Failed to find users", "userController => findUsers()", 5);
                    let apiResponse = response.generate(true, "Failed to find user", 500, null);
                    reject(apiResponse);
                } else if (check.isEmpty(userDetails)) {
                    logger.error("User not found", "userController => findUsers()", 5);
                    let apiResponse = response.generate(true, "User not found", 500, null);
                    reject(apiResponse);
                } else {
                    resolve(userDetails);
                    // console.log("userDetails", userDetails);
                }
            });
        });
    } // end of findTeamMember function

    let findCampaign = () => {
        console.log("findCampaign");
        return new Promise((resolve, reject) => {
            Campaign.findOne({campaignId: req.body.campaignId}, function (err, campaignDetails) {
                if (err) {
                    logger.error("Failed to find Campaign", "campaignController => findCampaign()", 5);
                    let apiResponse = response.generate(true, "Failed to find Campaign", 500, null);
                    reject(apiResponse);
                } else if (check.isEmpty(campaignDetails)) {
                    logger.error("Campaign not found", "campaignController => findCampaign()", 5);
                    let apiResponse = response.generate(true, "Campaign not found", 500, null);
                    reject(apiResponse);
                } else {
                    resolve(campaignDetails);
                    // console.log("userDetails", userDetails);
                }
            });
        });
    } // end of findTeamMember function

    let findBudget = () => {
        console.log("findBudget");
        return new Promise((resolve, reject) => {
            let date = new Date();
            date = (date.getFullYear()) +'-'+(date.getMonth() + 1);
            userMonthlyBudget.find({$and: [{teamMemberId: req.data.teamMemberId}, {month: date}]}, function (err, budgetDetails) {
                if (err) {
                    logger.error("Failed to find budget", "campaignController => findBudget()", 5);
                    let apiResponse = response.generate(true, err, 500, null);
                    reject(apiResponse);
                } else if (check.isEmpty(budgetDetails)) {
                    logger.error("User Budget is Not allocated.", "campaignController => findBudget()", 5);
                    let apiResponse = response.generate(true, "User Budget is Not allocated.", 500, null);
                    reject(apiResponse);
                } else {
                    resolve();
                }
            });
        });
    } // end of findBudget function

    let getAllMonthlyMemberBudget = () => {
        console.log("getAllMonthlyMemberBudget");
        return new Promise((resolve, reject) => {
            let date = new Date();
            date = (date.getFullYear()) +'-'+ (date.getMonth() + 1);
            CampaignActivity.aggregate([
                {$match: {$and: [{teamMemberId: req.data.teamMemberId}, {month: date}]}},
                {
                    $group: {
                        _id: "$teamMemberId",
                        totalamount: {$sum: "$amount"}
                    }
                }
            ], function (err, budgetDetails) {
                if (err) {
                    logger.error("Failed to find User Monthly budget", "budgetController => getAllMonthlyBudget()", 5);
                    let apiResponse = response.generate(true, err, 500, null);
                    reject(apiResponse);
                } else if (check.isEmpty(budgetDetails)) {
                    resolve();
                } else {
                    resolve(budgetDetails);
                }
            });
        });
    } // end of getAllMonthlyMemberBudget function

    let findMemberBudget = (budgetDetails) => {
        console.log("findMemberBudget");
        return new Promise((resolve, reject) => {
            let budgetDetailsAmount;
            if (budgetDetails !== undefined) {
                budgetDetailsAmount = (budgetDetails[0].totalamount) + Number(req.body.amount);
            } else {
                budgetDetailsAmount = Number(req.body.amount);
            }
            // console.log('budgetDetailsAmount', budgetDetailsAmount)
            let date = new Date();
            date = (date.getFullYear()) +'-'+(date.getMonth() + 1);
            userMonthlyBudget.find({$and: [{teamMemberId: req.data.teamMemberId}, {month: date}, {amount: {$lte: budgetDetailsAmount - 1}}]}, function (err, budgetDetails) {
                if (err) {
                    logger.error("Failed to find user budget", "budgetController => checkBudget()", 5);
                    let apiResponse = response.generate(true, err, 500, null);
                    reject(apiResponse);
                } else if (check.isEmpty(budgetDetails)) {
                    resolve();
                } else {
                    logger.error("Amount is extending the monthly budget Limit.", "budgetController => checkBudget()", 5);
                    let apiResponse = response.generate(true, "Amount is extending the monthly budget Limit.", 500, null);
                    reject(apiResponse);
                }
            });
        });
    } // end of findMemberBudget function

    let createcampaignActivity = () => {
        console.log("createCampaignActivity");
        return new Promise((resolve, reject) => {
            let body = {};
            let date = new Date()
            body['campaignActivityId'] = shortid.generate();
            body['campaignId'] = req.body.campaignId;
            body['amount'] = req.body.amount;
            body['description'] = req.body.description;
            body['month'] = (date.getFullYear()) +'-'+(date.getMonth() + 1);
            body['teamMemberId'] = req.data.teamMemberId;
            body['createdOn'] = date;
            CampaignActivity.create(body, function (err, response) {
                if (err) {
                    logger.error("Failed to create Campaign Activity", "campaignController => createCampaignActivity()", 5);
                    let apiResponse = response.generate(true, err, 500, null);
                    reject(apiResponse);
                } else {
                    let finalObject = response.toObject();
                    delete finalObject.__v;
                    resolve(finalObject);
                }
            });
        });
    } // end of createCampaignActivity function

    validatingInputs()
        .then(findTeamMember)
        .then(findCampaign)
        .then(findBudget)
        .then(getAllMonthlyMemberBudget)
        .then(findMemberBudget)
        .then(createcampaignActivity)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Create Campaign Activity Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of create Campaign Activity function

// get Campaign Id List function
let getCampaignIdList = (req, res) => {

    let findCampaignList = () => {
        console.log("findCampaignList");
        return new Promise((resolve, reject) => {
            Campaign.find({}, {}, {})
                .select('-__v -_id')
                .exec((err, campaignDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve campaign ", "campaignController => findCampaignList()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve campaign", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(campaignDetails)) {
                        logger.error("No campaign  found", "campaignController => findCampaignList()", 5);
                        let apiResponse = response.generate(true, "No campaign found", 500, null);
                        reject(apiResponse);
                    } else {
                        let campaign = []
                        let json;
                        let res;
                        campaignDetails.filter((y) => {
                            res = {
                                campaignId: y.campaignId,
                                campaignUrl: y.campaignUrl
                            }
                            campaign.push(res)
                        })
                        json = {
                            campaign: campaign
                        };
                        resolve(json);
                    }
                });
        });
    } // end of findCampaignList Functions

    findCampaignList()
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Campaign Id List Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of get CampaignId List function

// get Campaign Activity List function
let getCampaignActivityList = (req, res) => {

    let getSizeLimit = () => {
        console.log("getSizeLimit");
        return new Promise((resolve, reject) => {
            let skip = 0, limit = (req.query.pgSize) ? Number(req.query.pgSize) : 5;
            if (req.query.pg > 0) {
                skip = (limit) * (req.query.pg)
            }
            let data = {}
            data['skip'] = skip;
            data['limit'] = limit;
            resolve(data)
        });
    } // end of getSizeLimit function

    let findCampaignActivity = (data) => {
        console.log("findCampaignActivity");
        return new Promise((resolve, reject) => {
            CampaignActivity.find({teamMemberId: req.data.teamMemberId}, {}, {
                skip: data.skip,
                limit: data.limit,
                sort: {createdOn: 1}
            })
                .select('-__v -_id')
                .exec((err, campaignActivityDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve campaign Activity", "campaignController => findCampaignActivity()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve campaign Activity", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(campaignActivityDetails)) {
                        logger.error("No campaign Activity found", "campaignController => findCampaignActivity()", 5);
                        let apiResponse = response.generate(true, "No campaign Activity found", 500, null);
                        reject(apiResponse);
                    } else {
                        let final = [];
                        campaignActivityDetails.forEach((item) => {
                            let finalObject = item.toObject();
                            delete finalObject._id;
                            delete finalObject.__v;
                            final.push(finalObject)
                        })
                        resolve(final);
                    }
                });
        });
    } // end of findCampaignActivity Functions

    let findCampaignList = (data) => {
        console.log("findCampaignList");
        return new Promise((resolve, reject) => {
            Campaign.find({}, {}, {})
                .select('-__v -_id')
                .exec((err, campaignDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve campaign ", "campaignController => findCampaignList()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve campaign", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(campaignDetails)) {
                        logger.error("No campaign  found", "campaignController => findCampaignList()", 5);
                        let apiResponse = response.generate(true, "No  campaign found", 500, null);
                        reject(apiResponse);
                    } else {
                        let campaign = []
                        let json;
                        let res;
                        let final = [];
                        (data).filter((x) => {
                            campaignDetails.filter((y) => {
                                if (x.campaignId === y.campaignId) {
                                    x["campaignUrl"] = y.campaignUrl
                                    final.push(x)
                                }
                            })
                        })
                        campaignDetails.filter((y) => {
                            res = {
                                campaignId: y.campaignId,
                                campaignUrl: y.campaignUrl
                            }
                            campaign.push(res)
                        })
                        json = {
                            campaign: campaign,
                            campaignActivity: final
                        };
                        resolve(json);
                    }
                });
        });
    } // end of findCampaignList Functions

    let totalCampaignActivity = (final) => {
        console.log("totalCampaignActivity");
        return new Promise((resolve, reject) => {
            CampaignActivity.find({teamMemberId: req.data.teamMemberId}).count(function (err, cnt) {
                if (err) {
                    logger.error("Failed to retrieve campaign Activity", "campaignController => totalCampaignActivity()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve campaign Activity", 500, null);
                    reject(apiResponse);
                } else {
                    final['totalRecords'] = cnt;
                    resolve(final);
                }
            });
        });
    } // end of total Campaign Activity function

    getSizeLimit()
        .then(findCampaignActivity)
        .then(findCampaignList)
        .then(totalCampaignActivity)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Campaign Activity List Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of Campaign Activity List function

module.exports = {
    createCampaign: createCampaign,
    getCampaignList: getCampaignList,
    updateCampaign: updateCampaign,
    createCampaignActivity: createCampaignActivity,
    getCampaignActivityList: getCampaignActivityList,
    getCampaignIdList: getCampaignIdList,
}// end exports
