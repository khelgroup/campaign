const mongoose = require('mongoose');
const shortid = require('shortid');
const time = require('./../libs/timeLib');
const response = require('./../libs/responseLib')
const logger = require('./../libs/loggerLib');
const validateInput = require('../libs/paramsValidationLib')
const check = require('../libs/checkLib')
const {isNullOrUndefined} = require('util');
const tokenLib = require('../libs/tokenLib')


/* Models */
const UserModel = mongoose.model('User');
const tokenCol = mongoose.model('tokenCollection');

// create User function
let createUser = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if ((req.body.role === 'Manager') ? (req.body.password && req.body.name && req.body.role) : (req.body.password && req.body.name && req.body.role && req.body.managerId)) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "Parameters missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let checkManager = () => {
        console.log("checkManager");
        return new Promise((resolve, reject) => {
            if (req.body.role === 'Team Member') {
                UserModel.find({managerId: req.body.managerId}, function (err, managerDetails) {
                    if (err) {
                        logger.error("Failed to create user", "userController => checkManager()", 5);
                        let apiResponse = response.generate(true, err, 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(managerDetails)) {
                        logger.error("Manager not found", "userController => checkManager()", 5);
                        let apiResponse = response.generate(true, "Manager not found", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve();
                    }
                });
            } else {
                resolve();
            }
        });
    } // end of createUser function

    let createUsers = () => {
        console.log("createUser");
        return new Promise((resolve, reject) => {
            let body = {};
            if (req.body.role === 'Manager') {
                body['managerId'] = shortid.generate();
            } else {
                body['managerId'] = req.body.managerId;
                body['teamMemberId'] = shortid.generate();
            }
            body['name'] = req.body.name ? req.body.name : '';
            body['isActive'] = true;
            body['role'] = req.body.role;
            body['password'] = req.body.password;
            body['createdOn'] = new Date();
            UserModel.create(body, function (err, response) {
                if (err) {
                    logger.error("Failed to create user", "userController => createUsers()", 5);
                    let apiResponse = response.generate(true, err, 500, null);
                    reject(apiResponse);
                } else {
                    let finalObject = response.toObject();
                    delete finalObject._id;
                    delete finalObject.__v;
                    resolve(finalObject);
                }
            });
        });
    } // end of createUser function

    validatingInputs()
        .then(checkManager)
        .then(createUsers)
        .then((resolve) => {
            let apiResponse = response.generate(false, "User Created Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
}

// login User function
let loginUser = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.body.name && req.body.password && req.body.role) {
                resolve();
            } else {
                let apiResponse = response.generate(true, "password or name or role missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let findUsers = () => {
        console.log("findUsers");
        return new Promise((resolve, reject) => {
            let model;
            UserModel.find({$and: [{name: req.body.name}, {role: req.body.role}]}, function (err, userDetails) {
                if (err) {
                    logger.error("Failed to find users", "userController => findUsers()", 5);
                    let apiResponse = response.generate(true, "Failed to find user", 500, null);
                    reject(apiResponse);
                } else if (check.isEmpty(userDetails)) {
                    logger.error("User not found", "userController => findUsers()", 5);
                    let apiResponse = response.generate(true, "User not found", 500, null);
                    reject(apiResponse);
                } else {
                    resolve(userDetails[0]);
                    // console.log("userDetails", userDetails);
                }
            });
        });
    } // end of findUsers function

    let pwdMatch = (userDetails) => {
        console.log("pwdMatch");
        return new Promise((resolve, reject) => {
            let password = req.body.password
            userDetails.comparePassword(password, function (err, match) {
                if (err) {
                    logger.error("Wrong Passwordd", "userController => pwdMatch()", 5);
                    let apiResponse = response.generate(true, "Wrong Password", 500, null);
                    reject(apiResponse);
                } else {
                    if (match === true) {
                        resolve(userDetails);
                    } else {
                        logger.error("Wrong Password", "userController => pwdMatch()", 5);
                        let apiResponse = response.generate(true, "Wrong Password", 500, null);
                        reject(apiResponse);
                    }
                }
            });
        });
    } // end of pwdMatch function

    let generateToken = (user) => {
        console.log("generateToken");
        return new Promise((resolve, reject) => {
            tokenLib.generateToken(user, (err, tokenDetails) => {
                if (err) {
                    logger.error("Failed to generate token", "userController => generateToken()", 10);
                    let apiResponse = response.generate(true, "Failed to generate token", 500, null);
                    reject(apiResponse);
                } else {
                    let finalObject = user.toObject()
                    delete finalObject.__v;
                    tokenDetails.userId = user._id
                    tokenDetails.userDetails = finalObject;
                    resolve(tokenDetails);
                }
            });
        });
    }; // end of generateToken

    let saveToken = (tokenDetails) => {
        console.log("saveToken");
        return new Promise((resolve, reject) => {
            tokenCol.findOne({userId: tokenDetails.userId})
                .exec((err, retrieveTokenDetails) => {
                    if (err) {
                        let apiResponse = response.generate(true, "Failed to save token", 500, null);
                        reject(apiResponse);
                    }
                    // player is logging for the first time
                    else if (check.isEmpty(retrieveTokenDetails)) {
                        let newAuthToken = new tokenCol({
                            userId: tokenDetails.userId,
                            authToken: tokenDetails.token,
                            // we are storing this is due to we might change this from 15 days
                            tokenSecret: tokenDetails.tokenSecret,
                            tokenGenerationTime: time.now()
                        });

                        newAuthToken.save((err, newTokenDetails) => {
                            if (err) {
                                let apiResponse = response.generate(true, "Failed to save token", 500, null);
                                reject(apiResponse);
                            } else {
                                let responseBody = {
                                    authToken: newTokenDetails.authToken,
                                    userDetails: tokenDetails.userDetails
                                };
                                resolve(responseBody);
                            }
                        });
                    }
                    // user has already logged in need to update the token
                    else {
                        retrieveTokenDetails.authToken = tokenDetails.token;
                        retrieveTokenDetails.tokenSecret = tokenDetails.tokenSecret;
                        retrieveTokenDetails.tokenGenerationTime = time.now();
                        retrieveTokenDetails.save((err, newTokenDetails) => {
                            if (err) {
                                let apiResponse = response.generate(true, "Failed to save token", 500, null);
                                reject(apiResponse);
                            } else {
                                delete tokenDetails._id;
                                delete tokenDetails.__v;
                                let responseBody = {
                                    authToken: newTokenDetails.authToken,
                                    userDetails: tokenDetails.userDetails
                                };
                                resolve(responseBody);
                            }
                        });
                    }
                });
        });

    }; // end of saveToken

    validatingInputs()
        .then(findUsers)
        .then(pwdMatch)
        .then(generateToken)
        .then(saveToken)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Login Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of login user function

// logout user function
let logoutUser = (req, res) => {

    let validatingInputs = () => {
        console.log("validatingInputs");
        return new Promise((resolve, reject) => {
            if (req.body.userId) {
                resolve(req);
            } else {
                let apiResponse = response.generate(true, "userId missing", 400, null);
                reject(apiResponse);
            }
        });
    }; // end of validatingInputs

    let findRemove = () => {
        console.log("findRemove");
        return new Promise((resolve, reject) => {
            tokenCol.findOneAndRemove({userId: req.body.userId})
                .exec((err, nwetokenDetail) => {
                    if (err) {
                        logger.error("Error while remove token", "userController => findRemove()", 5);
                        let apiResponse = response.generate(true, "Error while remove token", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(nwetokenDetail)) {
                        logger.error("No Token Remove", "userController => findRemove()", 5);
                        let apiResponse = response.generate(true, "No Token Remove", 500, null);
                        reject(apiResponse);
                    } else {
                        resolve(nwetokenDetail);
                    }
                });
        });
    } // end of findRemove function

    validatingInputs()
        .then(findRemove)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Logout Successfully!!", 200, null);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of the logout function

// get Team Member List function
let getTeamMemberList = (req, res) => {

    // let validatingInputs = () => {
    //     console.log("validatingInputs");
    //     return new Promise((resolve, reject) => {
    //         if (req.query.userId) {
    //             resolve(req);
    //         } else {
    //             let apiResponse = response.generate(true, "userId missing", 400, null);
    //             reject(apiResponse);
    //         }
    //     });
    // }; // end of validatingInputs

    let findMember = () => {
        console.log("findMember");
        return new Promise((resolve, reject) => {
            UserModel.find({$and :[{managerId: req.data.managerId},{teamMemberId:{ $ne :""}}]})
                .select('-__v -_id')
                .exec((err, budgetDetails) => {
                    if (err) {
                        logger.error("Failed to retrieve member", "userController => findMember()", 5);
                        let apiResponse = response.generate(true, "Failed to retrieve member", 500, null);
                        reject(apiResponse);
                    } else if (check.isEmpty(budgetDetails)) {
                        logger.error("No member found", "userController => findMember()", 5);
                        let apiResponse = response.generate(true, "No member found", 500, null);
                        reject(apiResponse);
                    } else {
                        let final = [];
                        budgetDetails.forEach((item) => {
                            let finalObject = item.toObject();
                            // console.log('finalObject',finalObject)
                            delete finalObject._id;
                            delete finalObject.password;
                            delete finalObject.__v;
                            final.push(finalObject)
                        })
                        resolve(final);
                    }
                });
        });
    } // end of findMember Functions

    let totalMember = (final) => {
        console.log("totalMember");
        return new Promise((resolve, reject) => {
            UserModel.find({managerId: req.query.userId}).count(function (err, cnt) {
                if (err) {
                    logger.error("Failed to retrieve user", "userController => totalMember()", 5);
                    let apiResponse = response.generate(true, "Failed to retrieve user", 500, null);
                    reject(apiResponse);
                } else {
                    final['totalRecords'] = cnt;
                    resolve(final);
                }
            });
        });
    } // end of total Member function

    findMember()
        .then(totalMember)
        .then((resolve) => {
            let apiResponse = response.generate(false, "Get Team Member List Successfully!!", 200, resolve);
            res.send(apiResponse);
        })
        .catch((err) => {
            console.log(err);
            res.send(err);
            res.status(err.status);
        });
} // end of get Campaign List function

module.exports = {
    loginUser: loginUser,
    createUser: createUser,
    logoutUser: logoutUser,
    getTeamMemberList: getTeamMemberList,

}// end exports
