const express = require('express');
const router = express.Router();
const campaignController = require("./../../app/controllers/campaignController");
// const playerController = require("./../../app/controllers/playerController");
const appConfig = require("./../../config/appConfig")

const middleware = require('../middlewares/auth');

module.exports.setRouter = (app) => {

    let baseUrl = `${appConfig.apiVersion}/campaign`;

    // defining routes.

    // create Campaign(campaignUrl, description)
    app.post(`${baseUrl}/createCampaign`, middleware.isAuthorize, campaignController.createCampaign);

    // get All campaign List
    app.get(`${baseUrl}/getCampaignList`,middleware.isAuthorize, campaignController.getCampaignList);

    // update campaign(campaignUrl, description)
    app.post(`${baseUrl}/updateCampaign`,middleware.isAuthorize, campaignController.updateCampaign);

    // create Campaign Activity(teamMemberId, amount or description)
    app.post(`${baseUrl}/createCampaignActivity`, middleware.isAuthorize,campaignController.createCampaignActivity);

    // get All campaign Activity List
    app.get(`${baseUrl}/getCampaignActivityList`, middleware.isAuthorize, campaignController.getCampaignActivityList);

    // get All campaign Id List
    app.get(`${baseUrl}/getCampaignIdList`, middleware.isAuthorize, campaignController.getCampaignIdList);

}
