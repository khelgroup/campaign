import {Routes} from '@angular/router';

import {HomeComponent} from '../../home/home.component';
import {BudgetComponent} from "../../budget/budget.component";
import {CampaignComponent} from "../../campaign/campaign.component";
import {UsermonthlybudgetComponent} from "../../usermonthlybudget/usermonthlybudget.component";
import {CampaignActivityComponent} from "../../campaign-activity/campaign-activity.component";
import {MemberBudgetComponent} from "../../member-budget/member-budget.component";

export const AdminLayoutRoutes: Routes = [
    {path: 'dashboard', component: HomeComponent},
    {path: 'budget', component: BudgetComponent},
    {path: 'campaign', component: CampaignComponent},
    {path: 'usermonthlybudget', component: UsermonthlybudgetComponent},
    {path: 'campaign-activity', component: CampaignActivityComponent},
    {path: 'member-budget', component: MemberBudgetComponent},
];
