import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {LbdModule} from '../../lbd/lbd.module';
import {NguiMapModule} from '@ngui/map';

import {AdminLayoutRoutes} from './admin-layout.routing';
import {HomeComponent} from '../../home/home.component';
import {UserComponent} from '../../user/user.component';
import {TablesComponent} from '../../tables/tables.component';
import {TypographyComponent} from '../../typography/typography.component';
import {IconsComponent} from '../../icons/icons.component';
import {NotificationsComponent} from '../../notifications/notifications.component';
import {UpgradeComponent} from '../../upgrade/upgrade.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {BudgetComponent} from '../../budget/budget.component';
import {CampaignComponent} from '../../campaign/campaign.component';
import {AdminLayoutComponent} from "./admin-layout.component";
import {UsermonthlybudgetComponent} from "../../usermonthlybudget/usermonthlybudget.component";
import {CampaignActivityComponent} from "../../campaign-activity/campaign-activity.component";
import {MemberBudgetComponent} from "../../member-budget/member-budget.component";


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AdminLayoutRoutes),
        FormsModule,
        LbdModule,
        NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=YOUR_KEY_HERE'}),
        NgxDatatableModule
    ],
    declarations: [
        HomeComponent,
        UserComponent,
        TablesComponent,
        TypographyComponent,
        IconsComponent,
        NotificationsComponent,
        UpgradeComponent,
        BudgetComponent,
        CampaignComponent,
        UsermonthlybudgetComponent,
        CampaignActivityComponent,
        MemberBudgetComponent,
    ]
})

export class AdminLayoutModule {
}
