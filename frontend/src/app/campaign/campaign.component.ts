import {Component, OnInit, ViewChild} from '@angular/core';
import {UserService} from "../services/user.service";
import {ColumnMode} from "@swimlane/ngx-datatable";
import {CampaignService} from '../services/campaign.service';
import {NotificationService} from '../services/notification.service';

@Component({
    selector: 'app-campaign',
    templateUrl: './campaign.component.html',
    styleUrls: ['./campaign.component.scss']
})
export class CampaignComponent implements OnInit {
    @ViewChild('myTable') table: any;
    rows: any = [];
    timeout: any;
    totalSize;
    currentPage = 0;
    pageSize = 5;
    display = 'none';
    editBlock = false;
    campaignData: any = {};
    limitOptions = [
        {
            key: '5',
            value: 5
        },
        {
            key: '20',
            value: 20
        },
        {
            key: '50',
            value: 50
        }
    ];


    constructor(private campaignService: CampaignService,
                private notificationService: NotificationService) {
        // this.fetch(data => {
        //   this.rows = data;
        // });
    }


    ngOnInit() {
        this.getCampaign(this.currentPage, this.pageSize);
    }

    getCampaign(pageNumber, pageSize) {
        this.campaignService.getCamapign(pageNumber, pageSize)
            .subscribe((res) => {
                console.log('campaignRes', res);
                if (!res.error) {
                    this.rows = res.data.campaign;
                    this.totalSize = res.data.totalRecords;
                } else {
                    this.notificationService.showNotification(res.message, 'danger');
                }
            })
    }

    addCampaign() {
        this.display = 'block';
    }

    onPage(event) {
        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
            console.log('paged!', event);
        }, 100);
    }

    saveCampaign() {
        this.display = 'none';
        // let userID = localStorage.getItem('userId');
        // this.campaignData["userId"] = userID;
        this.campaignService.addCampaign(this.campaignData)
            .subscribe((res) => {
                if (!res.error) {
                    this.campaignData = {};
                    this.getCampaign(this.currentPage, this.pageSize);
                    this.notificationService.showNotification(res.message, 'success');
                    this.getCampaign(this.currentPage, this.pageSize);
                } else {
                    this.notificationService.showNotification(res.message, 'danger');
                    this.campaignData = {};
                    this.getCampaign(this.currentPage, this.pageSize);
                }
                console.log('res', res);
            })
    }

    onCloseHandled() {
        this.display = 'none';
    }

    onPageSizeChanged(event) {
        this.currentPage = 0;
        this.pageSize = event;
        this.getCampaign(this.currentPage, this.pageSize);
    }

    pageCallback(e) {
        this.getCampaign(e.offset, e.pageSize);
    }

}
