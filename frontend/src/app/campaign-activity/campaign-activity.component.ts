import {Component, OnInit, ViewChild} from '@angular/core';
// import {UserService} from "../services/user.service";
// import {ColumnMode} from "@swimlane/ngx-datatable";
import {CampaignService} from "../services/campaign.service";
import {NotificationService} from "../services/notification.service";
import {padNumber} from "@ng-bootstrap/ng-bootstrap/util/util";

@Component({
  selector: 'app-campaign-activity',
  templateUrl: './campaign-activity.component.html',
  styleUrls: ['./campaign-activity.component.scss']
})
export class CampaignActivityComponent implements OnInit {
  @ViewChild('myTable') table: any;
  rows = [];
  timeout: any;
  totalSize;
  campaignActivityId;
  currentPage = 0;
  pageSize = 5;
  display = 'none';
  editBlock = false;
  campaignActivityData: any = {};
  campaignOptions = [];
  limitOptions = [
    {
      key: '5',
      value: 5
    },
    {
      key: '20',
      value: 20
    },
    {
      key: '50',
      value: 50
    }
  ];


  constructor(private campaignService: CampaignService,
              private notificationService: NotificationService) {
  }


  ngOnInit() {
    this.campaignActivityId = localStorage.getItem('campaignActivityId');
    this.getCampaignActivity(this.currentPage, this.pageSize);
    this.getCampaignIdList();
  }

  getCampaignIdList() {
    this.campaignService.getCampaignIdList().subscribe((res) => {
      console.log('get Camapign Id list', res);
      if (!res.error) {
        this.campaignOptions = res.data.campaign;
      } else {
        this.notificationService.showNotification(res.message, 'danger');
      }

    });
  }

  getCampaignActivity(pageNumber, pageSize) {
    this.campaignService.getCamapignActivity(pageNumber, pageSize).subscribe((res) => {
      console.log('get Camapign Activity', res);
      if (!res.error) {
        this.rows = res.data.campaignActivity;
        this.totalSize = res.data.totalRecords;
      } else {
        this.notificationService.showNotification(res.message, 'danger');
      }

    });
  }

  addCampaign() {
    this.display = 'block';
    this.campaignActivityData.campaignId = "";
  }

  onPage(event) {
    clearTimeout(this.timeout)
    this.timeout = setTimeout(() => {
      console.log('paged!', event);
    }, 100);
  }


  saveCampaignActivity() {
    this.display = 'none';
    this.campaignActivityData['campaignActivityId'] = this.campaignActivityId;
    console.log('campaignActivityData', this.campaignActivityData);
    this.campaignService.addCampaignActivity(this.campaignActivityData)
        .subscribe((res) => {
          console.log('res', res);
          if (!res.error) {
            this.notificationService.showNotification(res.message, 'success');
            this.campaignActivityData = {};
            this.getCampaignActivity(this.currentPage, this.pageSize);
          } else {
            this.campaignActivityData = {};
            this.notificationService.showNotification(res.message, 'danger');
          }
        });
  }

  onCloseHandled() {
    this.display = 'none';
  }

  onPageSizeChanged(event) {
    this.currentPage = 0;
    this.pageSize = event;
    this.getCampaignActivity(this.currentPage, this.pageSize);
  }

  pageCallback(e) {
    this.getCampaignActivity(e.offset, e.pageSize);
  }

}

