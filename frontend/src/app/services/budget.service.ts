import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class BudgetService {

    api = environment.apiUrl;

    constructor(private http: HttpClient) {
    }

    addBudget(data) {
        return this.http.post<any>(this.api + '/budget/createBudget', data);
    }

    updateBudget(data) {
        return this.http.post<any>(this.api + '/budget/updateBudget', data);
    }

    getBudget() {
        return this.http.get<any>(this.api + '/budget/getBudget');
    }

    getTeam() {
        return this.http.get<any>(this.api + '/users/getTeamMemberList');
    }

    getusermonthlyBudget() {
        return this.http.get<any>(this.api + '/budget/getUserMonthlyBudgetList');
    }

    addusermonthlyBudget(data) {
        return this.http.post<any>(this.api + '/budget/createUserMonthlyBudget', data);
    }

    updateusermonthlyBudget(data) {
        return this.http.post<any>(this.api + '/budget/updateUserMonthlyBudget', data);
    }
    
    getMemberBudget() {
        return this.http.get<any>(this.api + '/budget/getperUserBudget');
    }

}
