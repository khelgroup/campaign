import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CampaignService {
  api = environment.apiUrl;
  constructor(private http: HttpClient) { }

  addCampaign(data) {
    return this.http.post<any>(this.api + '/campaign/createCampaign', data);
  }

  editCamapign(data) {
    return this.http.post<any>(this.api + '/users/logoutUser', data);
  }

  getCamapign(pageNumber, pageSize) {
    return this.http.get<any>(this.api + '/campaign/getCampaignList?pg=' + pageNumber + '&pgSize=' + pageSize);
  }

  addCampaignActivity(data) {
    return this.http.post<any>(this.api + '/campaign/createCampaignActivity', data);
  }

  getCamapignActivity(pageNumber, pageSize) {
    return this.http.get<any>(this.api + '/campaign/getCampaignActivityList?pg=' + pageNumber + '&pgSize=' + pageSize);
  }

  getCampaignIdList() {
    return this.http.get<any>(this.api + '/campaign/getCampaignIdList');
  }


}
