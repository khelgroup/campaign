import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class ActivityService {
  api = environment.apiUrl;
  constructor(private http: HttpClient) { }

  addActivity(data) {
    return this.http.post<any>(this.api + '/users/logoutUser', data);
  }

  editActivity(data) {
    return this.http.post<any>(this.api + '/users/logoutUser', data);
  }
}
