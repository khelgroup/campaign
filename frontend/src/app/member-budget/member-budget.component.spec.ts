import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberBudgetComponent } from './member-budget.component';

describe('MemberBudgetComponent', () => {
  let component: MemberBudgetComponent;
  let fixture: ComponentFixture<MemberBudgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MemberBudgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberBudgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
