import { Component, OnInit } from '@angular/core';
import {BudgetService} from "../services/budget.service";

@Component({
  selector: 'app-member-budget',
  templateUrl: './member-budget.component.html',
  styleUrls: ['./member-budget.component.scss']
})
export class MemberBudgetComponent implements OnInit {

  rows = [];
  constructor(private budgetService: BudgetService) { }

  ngOnInit() {
this.budgetService.getMemberBudget()
    .subscribe((res) => {
      console.log('response', res);
      if (!res.error) {
        this.rows = res.data;
      }
    })
  }

}
