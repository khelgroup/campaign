import {Component, OnInit, ViewChild} from '@angular/core';
import {DatatableComponent} from "@swimlane/ngx-datatable";
import {BudgetService} from "../services/budget.service";
import {NotificationService} from "../services/notification.service";

@Component({
    selector: 'app-budget',
    templateUrl: './budget.component.html',
    styleUrls: ['./budget.component.scss']
})
export class BudgetComponent implements OnInit {
    @ViewChild(DatatableComponent) myTable: DatatableComponent;
    budgetData: any = {};
    rows = [];
    display = 'none';
    editBlock = false;
    managerId;
    today;
    editDate: any = [];


    constructor(private budgetService: BudgetService,
                private notificationService: NotificationService) {
    }


    ngOnInit() {
        this.getBudget()
    }

    getBudget() {
        this.budgetService.getBudget()
            .subscribe((res) => {
                console.log('budgetres', res);
                if (!res.error) {
                    this.rows = res.data;
                } else {
                    this.notificationService.showNotification(res.message, 'danger');
                }
            });
    }

    addBudget() {
        this.display = 'block';
        this.editBlock = false;
        this.budgetData.date = new Date()

    }

    onCloseHandled() {
        this.display = 'none';
        this.budgetData = {};
        this.editDate = [];
        this.getBudget();
    }

    saveBudget() {
        this.display = 'none';
        this.budgetData['managerId'] = this.managerId;
        this.budgetService.addBudget(this.budgetData).subscribe((res) => {
            console.log('res', res);
            if (!res.error) {
                this.notificationService.showNotification(res.message, 'success');
                this.getBudget();
                this.budgetData = {};
            } else {
                this.budgetData = {};
                this.notificationService.showNotification(res.message, 'danger');
            }
        });
    }

    updateBudget() {
        this.display = 'none';
        this.editDate.push(this.budgetData.date);
        console.log('editDate', this.editDate)
        this.budgetData['twoDates'] = this.editDate;
        this.budgetData['budgetId'] = this.budgetData.budgetId;
        this.budgetService.updateBudget(this.budgetData).subscribe((res) => {
            console.log('res', res);
            if (!res.error) {
                this.notificationService.showNotification(res.message, 'success');
                this.getBudget();
                this.budgetData = {};
                this.editDate = [];
            } else {
                this.notificationService.showNotification(res.message, 'danger');
                this.budgetData = {};
                this.editDate = [];
                this.getBudget();
            }
        });
    }


    editBudget(data) {
        this.display = 'block';
        this.editBlock = true;
        this.budgetData = data;
        this.editDate.push(this.budgetData.date);
        console.log('in editConfiguration', data, this.editDate)
    }

}
