import {Component, OnInit} from '@angular/core';
import {UserService} from "../services/user.service";

declare const $: any;

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}


export const ROUTES: RouteInfo[] = [
    // {path: '/budget', title: 'Budget', icon: 'pe-7s-rocket', class: ''},
    // {path: '/campaign', title: 'Campaign', icon: 'pe-7s-rocket', class: ''},
    // {path: '/activity', title: 'Activity', icon: 'pe-7s-rocket', class: ''},
];




@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
    menuItems: any[];
    userRole;
    userId;

    constructor(private userService: UserService) {
    }

    ngOnInit() {
        this.userId = localStorage.getItem('userId');
        console.log('userId', this.userId);
        if (localStorage.getItem('role') === 'Manager') {
            while (ROUTES.length > 0) {
                ROUTES.pop();
            }
            ROUTES.push({path: '/budget', title: 'Budget', icon: 'pe-7s-rocket', class: ''})
            ROUTES.push({path: '/campaign', title: 'Campaign', icon: 'pe-7s-rocket', class: ''})
            ROUTES.push({path: '/usermonthlybudget', title: 'User Monthly Budget', icon: 'pe-7s-rocket', class: ''})
            // ROUTES.push({path: '/myteam', title: 'My Team', icon: 'pe-7s-rocket', class: ''})

        } else {
            while (ROUTES.length > 0) {
                ROUTES.pop();
            }
            // ROUTES.push({path: '/activity', title: 'Activity', icon: 'pe-7s-rocket', class: ''})
            ROUTES.push({path: '/campaign', title: 'Campaign', icon: 'pe-7s-rocket', class: ''})
            ROUTES.push({path: '/campaign-activity', title: 'Campaign Activity', icon: 'pe-7s-rocket', class: ''})
            ROUTES.push({path: '/member-budget', title: 'Budget', icon: 'pe-7s-rocket', class: ''})

        }
        this.menuItems = ROUTES.filter(menuItem => menuItem);
    }

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };

    logoutUser() {
        let data = {
            userId: this.userId
        };
        this.userService.logoutUser(data).
            subscribe((res) => {
            console.log('reslogout', res)
        }, error => {
                console.log('error', error)
        })
    }
}
