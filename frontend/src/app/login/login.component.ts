import {Component, OnInit} from '@angular/core';
import {UserService} from '../services/user.service';
import {NotificationService} from '../services/notification.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    user: any = {};
    roleOptions = [
        {
            key: 'Manager',
            value: 'Manager'
        },
        {
            key: 'Team Member',
            value: 'Team Member'
        }
    ];

    constructor(private userService: UserService,
                private notificationService: NotificationService,
                private router: Router) {
    }

    ngOnInit() {
        this.user.role = '';
        let accessToken = localStorage.getItem('token');
        console.log('accessToken', accessToken);
        if (!accessToken) {
            this.router.navigate(['/login']);
        } else {
            this.router.navigate(['/dashboard']);
        }
    }

    userLogin() {
        this.userService.login(this.user)
            .subscribe((response) => {
                console.log('this.user', response.data)
                if (!response.error) {
                    this.user = {};
                    localStorage.setItem('token', response.data.authToken);
                    localStorage.setItem('userId', response.data.userDetails._id);
                    localStorage.setItem('managerId', response.data.userDetails.managerId);
                    localStorage.setItem('role', response.data.userDetails.role);
                    this.router.navigate(['/dashboard']);
                    this.notificationService.showNotification(response.message, 'success');
                } else {
                    this.notificationService.showNotification(response.message, 'danger');
                }
            });
    }
}
