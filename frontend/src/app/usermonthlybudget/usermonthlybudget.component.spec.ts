import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsermonthlybudgetComponent } from './usermonthlybudget.component';

describe('UsermonthlybudgetComponent', () => {
  let component: UsermonthlybudgetComponent;
  let fixture: ComponentFixture<UsermonthlybudgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsermonthlybudgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsermonthlybudgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
