import {Component, OnInit, ViewChild} from '@angular/core';
import {DatatableComponent} from "@swimlane/ngx-datatable";
import {BudgetService} from "../services/budget.service";
import {NotificationService} from "../services/notification.service";

@Component({
    selector: 'app-usermonthlybudget',
    templateUrl: './usermonthlybudget.component.html',
    styleUrls: ['./usermonthlybudget.component.scss']
})
export class UsermonthlybudgetComponent implements OnInit {

    @ViewChild(DatatableComponent) myTable: DatatableComponent;
    userBudgetData: any = {};
    rows = [];
    display = 'none';
    editBlock = false;
    managerId;
    totalSize;
    teamMemberId = [];
    teamOptions: any = [];
    editDate: any = [];

    constructor(private budgetService: BudgetService,
                private notificationService: NotificationService) {
    }

    addUserBudget() {
        this.display = 'block';
        this.userBudgetData.teamMemberId = "";
        this.editBlock = false;
        this.userBudgetData.date = new Date()
    }

    getTeam() {
        this.budgetService.getTeam()
            .subscribe((res) => {
                console.log('budgetres', res);
                if (!res.error) {
                    this.teamOptions = res.data;
                } else {
                    this.notificationService.showNotification(res.message, 'danger');
                }
            });
    }

    onCloseHandled() {
        this.display = 'none';
        this.getUserBudget();
        this.editDate = [];
        this.userBudgetData = {}
    }

    saveBudget() {
        this.display = 'none';
        this.budgetService.addusermonthlyBudget(this.userBudgetData)
            .subscribe((res) => {
                console.log('res', res);
                if (!res.error) {
                    this.notificationService.showNotification(res.message, 'success');
                    this.getUserBudget();
                    this.userBudgetData = {};
                } else {
                    this.userBudgetData = {};
                    this.notificationService.showNotification(res.message, 'danger');
                }
            });
    }

    updateBudget() {
        this.display = 'none';
        console.log('teamMemberId', this.userBudgetData.teamMemberId);
        this.editDate.push(this.userBudgetData.date);
        console.log('editDate', this.editDate)
        this.userBudgetData['twoDates'] = this.editDate;
        this.userBudgetData['budgetId'] = this.userBudgetData.budgetId;
        this.budgetService.updateusermonthlyBudget(this.userBudgetData).subscribe((res) => {
            console.log('res', res);

            if (!res.error) {
                this.notificationService.showNotification(res.message, 'success');
                this.getUserBudget();
                this.userBudgetData = {};
                this.editDate = [];
            } else {
                this.getUserBudget();
                this.userBudgetData = {};
                this.editDate = [];
                this.notificationService.showNotification(res.message, 'danger');
            }
        });
    }

    editBudget(data) {
        this.display = 'block';
        this.editBlock = true;
        this.userBudgetData = data;
        this.editDate.push(this.userBudgetData.date);
        console.log('in editConfiguration', data)
    }

    ngOnInit() {
        this.managerId = localStorage.getItem('managerId');
        console.log('managerId', this.managerId);
        this.userBudgetData.teamMemberId = '';
        this.getUserBudget();
        this.getTeam();
    }

    getUserBudget() {
        this.budgetService.getusermonthlyBudget().subscribe((res) => {
            console.log('budgetres', res);
            if (!res.error) {
                this.rows = res.data.userBudget;
                this.totalSize = res.data.totalRecords;
            } else {
                this.notificationService.showNotification(res.message, 'danger');
            }
        });
    }
}




